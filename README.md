# Fix IAM roles of vms to enable CWAgent

After you use the quick setup for systems manager installation, sometimes there can be some vms
for which the quick setup has failed to attach the suitable IAM role. As a result the CWAgent
will not be able to push metrics to the CloudWatch. More specifically 

- If a vm already has a role attached, the quick setup does not attach the required polices to
current role
- Even if a vm does not currently have any role but a role attachment and detachment have been done
in the past, the quick setup does not attach the required role to it.

This script takes care of attaching the required role or modifying the exiting role by adding
required policies.

### Steps
The following steps installs CWAgent in one aws account at a time. So if you want to use it for
more than one account, you have to do the steps below for each account at a time.

1. You can open the AWS CloudShell from the account where you want to install the agent and move to 
step
2. CloudShell will use the logged in user's credentials.  
But if you want to run it in local
then make sure to have python3.7+ installed and boto3 (v1.17+) in your python environment. Run
``aws configure`` and give your credentials, default region (the script will for all the  regions)
etc.
3. Clone the repository
``git clone https://bitbucket.org/leankloud/aws-systems-manager-installer.git``
4. Go into aws-systems-manager-installer directory ``cd aws-systems-manager-installer``
5. Then run
``python3 cwagent_iam_policy_association.py``  
If you are running on a local shell and using a virtual environment it might be `python`
instead of `python3`
6. Go to aws console > Systems Manager > then use the quick setup to install the agents.


### Troubleshooting
The roles being attached to each vm can be found in log output from the script which will be in logs 
directory.
If you do not see CWAgent running after the above steps please wait at least an hour after running
the quick setup (step 5). If there are still problems with some vms please make sure you are running
[a compatible OS](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/Install-CloudWatch-Agent.html)
and manually check roles attached to the vm. It should contain at least these two following 
policies for CWAgent to work.

- ``arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy``
- ``arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore``