# Automation tool for LeanKloud client to setup necessary
# prerequisite agent installations - SSM agent CW agent and
# enabling ConfigService

import argparse
import boto3
import json
import time

from botocore.exceptions import ClientError
from collections import defaultdict

# Name of the roles to be use by the below services to perform its services
# role to attach to instance profile so that installed SSM agent will work this role is
# created by Leankloud, and will create one if not exists or uses already created one
LK_SSM_ROLE_NAME = "LeanKloudRoleForInstanceSSMAutomation"
# Role fot config service so that it can record configuration changes
LK_CONFIG_ROLE_NAME = "LeanKloudRoleForConfigService"

# A role must assume a role to get temporary credentials from STS Security Token Service
# https://docs.aws.amazon.com/systems-manager/latest/userguide
# /automation-permissions.html#automation-role
SSM_ASSUME_ROLE_POLICY = {
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Principal": {
            "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
    }]
}
# https://docs.aws.amazon.com/config/latest/developerguide
# /iamrole-permissions.html#iam-trust-policy
CONFIG_ASSUME_ROLE_POLICY = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": "config.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}

# Details for the role
LK_ROLE_PATH = '/leankloud/'
LK_SSM_ROLE_DESCRIPTION = "Role created by LeanKloud to install SSM agent in EC2 server"
LK_CONFIG_ROLE_DESCRIPTION = "Role created by LeanKloud to enable config service"

# Policies to be attached
SSM_POLICIES = [
    'arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy',
    'arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore',
    'arn:aws:iam::aws:policy/AmazonSSMPatchAssociation',
]
# policies required for the config role
CONFIG_POLICIES = [
    'arn:aws:iam::aws:policy/service-role/AWSConfigRole'
]

# Config service details
LK_CONFIG_NAME = "LK-Config-Recorder"

# bucket to store the recorded details must be unique across users,
# so it must be stored suffixed by account id
CONFIG_LOG_BUCKET = 'config-bucket-{}'

# documents in SSM to send through run command
# https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-ssm-docs.html
INSTALL_CW_DOCUMENT = "AWS-ConfigureAWSPackage"
MANAGE_CW_AGENT_DOCUMENT = "AmazonCloudWatch-ManageAgent"
# To configure installed CW agent with the required metrics we need to pass a configuration
# file, which is created in this directory with metrics that leankloud expects to be pushed
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/
# create-cloudwatch-agent-configuration-file.html
CW_CONFIGURATION_FILE_PATH = "{os_type}_cloudwatch_config.json"
# configuration file stored as parametr store of SSM to configure CW in instances
LK_CW_CONFIG_PARAMETER_NAME = {
    'windows': 'LeanKloud-AWSCloudwatchConfig-Windows',
    'linux': 'LeanKloud-AWSCloudwatchConfig-Linux'
}
CW_CONF_PARAM_DESC = 'CloudWatch configuration file to be used by AmazonCloudWatchAgent'

# Policy for the Config bucket so that config service will be able to access this resource
# must be formatted with appropriate name of the config_bucket and account id
# https://docs.aws.amazon.com/config/latest/developerguide/s3-bucket-policy.html
S3_BUCKET_POLICY_FOR_CONFIG = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSConfigBucketPermissionsCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "config.amazonaws.com"
                ]
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::{config_bucket}"
        },
        {
            "Sid": "AWSConfigBucketExistenceCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "config.amazonaws.com"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::{config_bucket}"
        },
        {
            "Sid": " AWSConfigBucketDelivery",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "config.amazonaws.com"
                ]
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::{config_bucket}/AWSLogs/{account}/Config/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}


class IamResourceHelper(object):
    """Helper class which uilizes the passed class's methods get or
    create appropriate IAM resources
    """

    def __init__(self, service_object):
        """
        Args:
            service_object (object): service manager class which has methods
            to create and get IAM resources
        """
        self.service_object = service_object

    def _get_method(self, resource_name, resource_type, operation):
        return getattr(
            self.service_object,
            "_{}_{}_{}".format(operation, resource_name, resource_type)
        )

    def _get_or_create_iam_resource(self, resource_name, resource_type):
        # create policy with permissions to SSM
        # or retrieve if already exists
        get_resource = self._get_method(resource_name, resource_type, 'get')
        create_resource = self._get_method(resource_name, resource_type, 'create')
        resource = get_resource()
        if not resource:
            resource = create_resource()
        return resource

    def _add_policies_to_role(self, role, policies):
        for policy in policies:
            role.attach_policy(PolicyArn=policy)


class ConfigServiceManager(object):
    """ Involves creating role for ConfigService and starting a recorder
    in ConfigService for all types of resources in the region

    Attributes:
        role (boto3.IAM.role): IAM role object for configservice
    """

    def __init__(self, key_id, secret):
        """
        Args:
            key_id (str): AWS credentials key_id
            secret (str): AWS credentials secret
        """
        self.sess = boto3.Session(aws_access_key_id=key_id, aws_secret_access_key=secret)
        self.iam = self.sess.resource('iam')
        self.iam_helper = IamResourceHelper(self)
        self.role = self.iam_helper._get_or_create_iam_resource("config", "role")
        self.role = self.iam_helper._get_or_create_iam_resource("config", "role")
        self.account_id = self.sess.client('sts').get_caller_identity().get('Account')
        s3_res = self.sess.resource('s3')
        s3_client = self.sess.client('s3')
        # use the s3 bucket if exists, else create one with policy
        # such that ConfigService can use it as delivery channel
        bucket_name = CONFIG_LOG_BUCKET.format(self.account_id)
        for stmt in S3_BUCKET_POLICY_FOR_CONFIG['Statement']:
            stmt['Resource'] = stmt['Resource'].format(
                config_bucket=bucket_name,
                account=self.account_id
            )
        self.s3_bucket = s3_res.Bucket(bucket_name)
        try:
            s3_client.head_bucket(Bucket=bucket_name)
        except ClientError:
            s3_client.create_bucket(Bucket=bucket_name)
            self.s3_bucket = s3_res.Bucket(bucket_name)
            s3_policy = self.s3_bucket.Policy()
            s3_policy.put(Policy=json.dumps(S3_BUCKET_POLICY_FOR_CONFIG))
        else:
            self.s3_bucket = s3_res.Bucket(bucket_name)

    def _create_config_role(self):
        # Create role for configservice
        role = self.iam.create_role(
            Path=LK_ROLE_PATH,
            RoleName=LK_CONFIG_ROLE_NAME,
            AssumeRolePolicyDocument=json.dumps(CONFIG_ASSUME_ROLE_POLICY),
            Description=LK_CONFIG_ROLE_DESCRIPTION,
        )
        self.iam_helper._add_policies_to_role(role, CONFIG_POLICIES)
        return role

    def _get_config_role(self):
        # see if lk policy is already existant in client system
        role = self.iam.Role(LK_CONFIG_ROLE_NAME)
        try:
            role.reload()
        except self.iam.meta.client.exceptions.NoSuchEntityException:
            return None
        return role

    def enable_config_in_region(self, region):
        """ Checks for existing config recorders in the region, if exists but
        not recording all the LeanKloud prescribed services, updates the recorder
        if does not exist, creates one.

        Args:
            region (str): Region to enable config service in

        """
        recorder_name = LK_CONFIG_NAME
        client = self.sess.client('config', region_name=region)
        recorders = client.describe_configuration_recorders()
        # check if the current enabled recorders are enabled for all resources
        for recorder in recorders['ConfigurationRecorders']:
            rec_grp = recorder['recordingGroup']
            if rec_grp['allSupported']:
                print(recorder['name'], "Recorder found for all supported services")
                recorder_name = recorder['name']
                break
        else:
            print("Creating config recorder in region {}".format(region))
        client.put_configuration_recorder(
            ConfigurationRecorder={
                'name': recorder_name,
                'roleARN': self.role.arn,
                'recordingGroup': {
                    'allSupported': True
                }
            }
        )
        client.put_delivery_channel(
            DeliveryChannel={
                'name': 'config-s3',
                's3BucketName': CONFIG_LOG_BUCKET.format(self.account_id)
            }
        )
        client.start_configuration_recorder(
            ConfigurationRecorderName=recorder_name
        )


class Ec2SSMProfileManager(object):
    """ Manages creating and associating instance profiles for EC2
    that are required for SSM Agent to work in EC2

    Attributes:
        ec2_client (boto3.client): Description
        lk_profile (boto3.IAM.profile): Profile required by SSM agent
        lk_role (boto3.IAM.role): Description: Role required by SSM agent
    """

    def __init__(self, key_id, secret):
        """
        Args:
            key_id (str): AWS credentials key_id
            secret (str): AWS credentials secret
        """
        self.sess = boto3.Session(aws_access_key_id=key_id, aws_secret_access_key=secret)
        self.iam = self.sess.resource('iam')
        self.iam_helper = IamResourceHelper(self)
        self.lk_role = self.iam_helper._get_or_create_iam_resource("ssm", "role")
        self.lk_profile = self.iam_helper._get_or_create_iam_resource("ssm", "profile")
        time.sleep(5)

    def associate(self, inst_md):
        """Associates the profile to the Ec2 instance
        creates a profile if not associated to the EC2
        if associated already checks if the attached profile has all the policies
        if not attaches the required policies to the existing role

        Args:
            inst_md (dict): EC2 instance metadata from describe_instances call

        Returns:
            resp (dict): 'status' key contains the status of the association

        """
        profile = self._profile_attached_to_instance(inst_md)
        profile = self._get_ssm_profile(profile)
        resp = {'status': 'associating'}
        if profile:
            # get the role attached to instance
            role = self._profile_role(profile)
            # check if the policies in the role are sufficient
            if self._contains_ssm_policies(role):
                resp['status'] = 'already associated'
            else:
                self.iam_helper._add_policies_to_role(role, SSM_POLICIES)
        else:
            profile = self.lk_profile
        print("attaching profile to instance: ", profile.name)
        self._attach_profile_to_instance(inst_md, profile)
        return resp

    def update_ec2_client(self, ec2_client):
        """Update the ec2_client attribute to the respective region client

        Args:
            ec2_client (boto3.client): EC2 client object of a region
        """
        self.ec2_client = ec2_client

    def _attach_profile_to_instance(self, inst_md, profile):
        if inst_md['State']['Name'] == 'terminated':
            return False
        profile_a = ec2_client.describe_iam_instance_profile_associations(
            Filters=[
                {
                    'Name': 'instance-id',
                    'Values': [
                        inst_md['InstanceId'],
                    ]
                },
            ]
        )['IamInstanceProfileAssociations']
        for association in profile_a:
            if association['State'] == 'associated':
                association_id = association['AssociationId']
                _ = ec2_client.disassociate_iam_instance_profile(
                    AssociationId=association_id
                )
        resp = ec2_client.associate_iam_instance_profile(
            IamInstanceProfile={
                'Arn': profile.arn,
                'Name': profile.name
            },
            InstanceId=inst_md['InstanceId']
        )
        status = resp['IamInstanceProfileAssociation']['State']
        if status != 'associating' or status != 'associated':
            return False
        return True

    def _create_ssm_role(self):
        """
        Create LeanKloud role for SSM automation
        Returns:
            (IAM.Role)
        """
        role = self.iam.create_role(
            Path=LK_ROLE_PATH,
            RoleName=LK_SSM_ROLE_NAME,
            AssumeRolePolicyDocument=json.dumps(SSM_ASSUME_ROLE_POLICY),
            Description=LK_SSM_ROLE_DESCRIPTION,
        )
        self.iam_helper._add_policies_to_role(role, policies=SSM_POLICIES)
        return role

    def _get_ssm_role(self):
        # see if lk policy is already existant in client system
        role = self.iam.Role(LK_SSM_ROLE_NAME)
        try:
            role.reload()
        except self.iam.meta.client.exceptions.NoSuchEntityException:
            return None
        return role

    def _create_ssm_profile(self):
        profile = self.iam.create_instance_profile(
            InstanceProfileName=LK_SSM_ROLE_NAME,
            Path=LK_ROLE_PATH
        )
        self._add_role_to_profile(self.lk_role, profile)
        return profile

    def _get_ssm_profile(self, profile=None):
        if not profile:
            profile = self.iam.InstanceProfile(LK_SSM_ROLE_NAME)
        try:
            profile.reload()
        except self.iam.meta.client.exceptions.NoSuchEntityException:
            return None
        return profile

    def _add_role_to_profile(self, role, profile):
        """ replace existing role in the profile
        Args:
            role (IAM.Role):
            profile (IAM.InstanceProfile)
        """
        profile.add_role(RoleName=role.name)

    def _profile_attached_to_instance(self, inst_md):
        # get the instance profile attached to the instance
        # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/iam-roles-for-amazon-ec2.html
        if 'IamInstanceProfile' not in inst_md:
            return None
        profile_name = inst_md['IamInstanceProfile']['Arn'].split('/')[-1]
        return self.iam.InstanceProfile(profile_name)

    def _contains_ssm_policies(self, role):
        """ Check if a role has policies for SSM agent to work
        Returns true if all plocies required for SSM are included
        Args:
            role (IAM.Role)
        """
        policies_in_role = {p: False for p in SSM_POLICIES}
        role_policy_iterator = role.attached_policies.all()
        for policy in role_policy_iterator:
            if policy.arn in policies_in_role:
                policies_in_role[policy.arn] = True
        return all(policies_in_role.values())

    def _profile_role(self, profile):
        """
        get the role attached to the instance profile
        An instance profile can contain only one role
        https://docs.aws.amazon.com/IAM/latest/UserGuide/
        id_roles_use_switch-role-ec2_instance-profiles.html#instance-profiles-manage-cli-api
        """
        return profile.roles[0]


class SSMAgentManager(object):
    """Manages operations to do with installed SSM agent in instances
    installing CW agent, storing parameters for configuring CW agent
    sending commands to instances to configure or start the agent

    Attributes:
        sess (boto3.Session): boto3 Session object with credentials loaded
        ssm (boto3.client): SSM boto3 client object
    """

    def __init__(self, sess):
        self.sess = sess
        self.ssm = self.sess.client('ssm')

    def instances(self):
        """Returns list fo instances on which ssm is installed

        Returns:
            list: instance ids on which ssm agent is installed and active
        """
        resp = self.ssm.describe_instance_information()
        return resp['InstanceInformationList']

    def start_cloudwatch_agent(self, arg_instance_ids, update_configuration=False):
        # install cw agent, configure them to push right metrics and start them
        instances = self.instances()
        vm_platforms = defaultdict(list)
        for inst in instances:
            if arg_instance_ids and inst['InstanceId'] not in arg_instance_ids:
                continue
            vm_platforms[inst['PlatformType']].append(inst['InstanceId'])
        if not vm_platforms:
            print('No instances to continue')
            return
        self.install_cw_agent(vm_platforms)
        self.configure_cw_agent(vm_platforms, update_configuration)
        self.enable_cw_agent(vm_platforms)

    def install_cw_agent(self, vm_platforms):
        """Installs the CW agent in the Virtual machine using SSM send command

        Args:
            vm_platforms (dict): keys as os type (Windows or Linux)
        """
        instances = []
        for os in vm_platforms:
            instances.extend(vm_platforms[os])
        print("installing CW agent in instances {}".format(instances))
        resp = self.ssm.send_command(
            InstanceIds=instances,
            DocumentName=INSTALL_CW_DOCUMENT,
            Parameters={
                'action': ['Install'],
                'name': ['AmazonCloudWatchAgent']
            },
            CloudWatchOutputConfig={
                'CloudWatchOutputEnabled': True
            }
        )
        return resp

    def enable_cw_agent(self, vm_platforms):
        """Enables/starts the installed CW aent in the VM

        Args:
            vm_platforms (dict): keys as os type (Windows or Linux)
        """
        for os, vms in vm_platforms.items():
            print("enabling CW agent in {} platform VMs {}".format(os, vms))
            self.ssm.send_command(
                InstanceIds=vms,
                DocumentName=MANAGE_CW_AGENT_DOCUMENT,
                Parameters={
                    'action': ['start'],
                    'optionalConfigurationSource': ['ssm']
                },
                CloudWatchOutputConfig={
                    'CloudWatchOutputEnabled': True
                }
            )

    def _put_parameter(self, os, overwrite):
        with open(CW_CONFIGURATION_FILE_PATH.format(os_type=os), 'r') as cnf:
            try:
                self.ssm.put_parameter(
                    Name=LK_CW_CONFIG_PARAMETER_NAME[os],
                    Description=CW_CONF_PARAM_DESC,
                    Value=cnf.read(),
                    Type='String',
                    Overwrite=overwrite,
                    Tier='Standard'
                )
                # print(resp)
            except self.ssm.exceptions.ParameterAlreadyExists:
                return

    def configure_cw_agent(self, vm_platforms, update_configuration=False):
        """Configure the installed CW agent to collect relevant or required metric
        according to a config file based on OS (Windows or Linux)
        Uses the json file present in the directory according to the OS
        https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/
        create-cloudwatch-agent-configuration-file.html

        Args:
            vm_platforms (dict): keys as os type (Windows or Linux)
            update_configuration (bool, optional): True, to overwrite
                already existing configuration
        """
        for os, vms in vm_platforms.items():
            self._put_parameter(platform, update_configuration)
            print("configuring CW agent in {} platform VMs {}".format(os, vms))
            self.ssm.send_command(
                InstanceIds=vms,
                DocumentName=MANAGE_CW_AGENT_DOCUMENT,
                Parameters={
                    'action': ['configure'],
                    'optionalConfigurationSource': ['ssm'],
                    'optionalConfigurationLocation': [LK_CW_CONFIG_PARAMETER_NAME[os.lower()]],
                    'optionalRestart': ['yes']
                },
                CloudWatchOutputConfig={
                    'CloudWatchOutputEnabled': True
                }
            )


if __name__ == '__main__':
    args_p = argparse.ArgumentParser()
    args_p.add_argument(
        '-k', '--key_id',
        required=True, help='key id of the AWS account access keys'
    )
    args_p.add_argument(
        '-s', '--secret',
        required=True, help='secret key of the AWS account access keys'
    )
    args_p.add_argument('-i', '--instance_ids', nargs='+', help='asset ids to install SSM agent')
    args_p.add_argument('-r', '--regions', nargs='+', help='list of regions to install SSM agent')
    args_p.add_argument('--update_cw_conf', action="store_true", help='Updates the CW agent\
                         configuration file, include this when configuration file is updated')
    args = args_p.parse_args()
    if args.regions:
        regions = args.regions
    else:
        regions = boto3.session.Session().get_available_regions('ec2')
    profile_associator = Ec2SSMProfileManager(args.key_id, args.secret)
    config_mngr = ConfigServiceManager(args.key_id, args.secret)
    for region in regions:
        sess = boto3.Session(
            aws_access_key_id=args.key_id,
            aws_secret_access_key=args.secret,
            region_name=region
        )
        ec2_client = sess.client('ec2')
        profile_associator.update_ec2_client(ec2_client)
        # config service must be enabled in all regions
        config_mngr.enable_config_in_region(region)
        ssm_manager = SSMAgentManager(sess)
        try:
            ec2_list = ec2_client.describe_instances()
        except ClientError as e:
            print(e)
            continue
        profile_associated = []
        if len(ec2_list['Reservations']) > 0:
            for res in ec2_list['Reservations']:
                for ec2 in res['Instances']:
                    inst_id = ec2['InstanceId']
                    if args.instance_ids and inst_id not in args.instance_ids:
                        continue
                    inst_status = ec2['State']['Name']
                    if inst_status != 'running':
                        print("{} is in {} state. Skipping".format(inst_id, inst_status))
                    platform = ec2.get('Platform', 'linux')
                    resp = profile_associator.associate(ec2)
                    profile_associated.append(inst_id)

        print("Profile Associated for instances")
        # wait for instances with already installed ssm agent to connect with remote
        # after associating the right profile
        time.sleep(10)
        ssm_instances = [vm['InstanceId'] for vm in ssm_manager.instances()]
        # if the instance profile is associated, yet it does not reflect
        # as ssm installed, then SSM is not installed or not working in that instance
        for inst in profile_associated:
            if inst not in ssm_instances:
                print(inst, "has no SSM agent")
            else:
                print("Starting CW agent in", inst)
        # if there are ssm installed instances, then install or enable cloudwatch agent
        if len(ssm_instances) > 0:
            ssm_manager.start_cloudwatch_agent(arg_instance_ids=args.instance_ids)
        print("Exit")
