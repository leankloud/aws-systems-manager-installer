import json
import logging
import os
from time import sleep

import boto3
import botocore

from utils import LK_ROLE_PATH, LK_SSM_ROLE_NAME, SSM_ASSUME_ROLE_POLICY, LK_SSM_ROLE_DESCRIPTION, SSM_POLICIES, \
    RegionUnavailableError

os.makedirs('logs', exist_ok=True)
logging.basicConfig(filename='logs/cwagent_association.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)

logger = logging.getLogger()


class EC2CWRoleAssociationAgent:
    def __init__(self, region):
        self.ec2_iam_profiles_associations = []
        self.region = region
        self.sess = boto3.Session(region_name=region)
        self.iam_resource = self.sess.resource('iam')
        self.ec2_client = self.sess.client('ec2')
        self.iam_profiles_map = {}
        self.ec2s_with_valid_profile = set()
        self.ec2s_without_profile = set()
        self.ec2s_with_invalid_profile = set()
        self.lk_profile = self.create_profile()
        self.unsuccessful_instances = {}

    @staticmethod
    def available_ec2_regions():
        sess = boto3.Session()
        return sess.get_available_regions('ec2')

    def validate_region_availability(self):
        logger.info(f'Using region {self.region}')
        try:
            self.ec2_client.describe_instances(DryRun=True)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] != 'DryRunOperation':
                logger.debug(e)
                raise RegionUnavailableError(f'Access denied for {self.region}')

    def create_roles(self):
        try:
            lk_role = self.iam_resource.create_role(
                Path=LK_ROLE_PATH,
                RoleName=LK_SSM_ROLE_NAME,
                AssumeRolePolicyDocument=json.dumps(SSM_ASSUME_ROLE_POLICY),
                Description=LK_SSM_ROLE_DESCRIPTION,
            )
            self.attach_polices(lk_role)
        except self.iam_resource.meta.client.exceptions.EntityAlreadyExistsException:
            lk_role = self.iam_resource.Role(LK_SSM_ROLE_NAME)
            logger.info(f'Role {lk_role.name} already exists.')
            if not self.contains_ssm_policies(lk_role):
                self.attach_polices(lk_role)
        return lk_role

    def attach_polices(self, role):
        for policy_arn in SSM_POLICIES:
            role.attach_policy(PolicyArn=policy_arn)

    def create_profile(self):
        lk_role = self.create_roles()
        try:
            lk_profile = self.iam_resource.create_instance_profile(
                InstanceProfileName=LK_SSM_ROLE_NAME,
                Path=LK_ROLE_PATH
            )
            lk_profile.add_role(RoleName=lk_role.name)
        except self.iam_resource.meta.client.exceptions.EntityAlreadyExistsException:
            lk_profile = self.iam_resource.InstanceProfile(LK_SSM_ROLE_NAME)
            logger.info(f'Profile {lk_profile.name} already exists.')
            if lk_role not in lk_profile.roles:
                logger.info(f'Attaching role {lk_role.name} to {lk_profile.name}')
                lk_profile.add_role(lk_role)
        sleep(10)
        return lk_profile

    def detach_profile(self, inst_id):
        profile = self.ec2_client.describe_iam_instance_profile_associations(
            Filters=[
                {
                    'Name': 'instance-id',
                    'Values': [
                        inst_id,
                    ]
                },
            ]
        )['IamInstanceProfileAssociations']
        for association in profile:
            if association['State'] == 'associated':
                association_id = association['AssociationId']
                self.ec2_client.disassociate_iam_instance_profile(
                    AssociationId=association_id
                )
                print(f'Detached profile for instance {inst_id}')

    def contains_ssm_policies(self, role):
        """ Check if a role has policies for SSM agent to work
        Returns true if all policies required for SSM are included
        Args:
            role (IAM.Role)
        """
        policies_in_role = {p: False for p in SSM_POLICIES}
        role_policy_iterator = role.attached_policies.all()
        for policy in role_policy_iterator:
            if policy.arn in policies_in_role:
                policies_in_role[policy.arn] = True
        return all(policies_in_role.values())

    def fetch_instance_profile_association(self):

        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_iam_instance_profile_associations
        logger.info(f'Fetching ec2 iam profile association details for region {self.region}')
        resp = self.ec2_client.describe_iam_instance_profile_associations()
        self.ec2_iam_profiles_associations = resp['IamInstanceProfileAssociations']
        while 'NextToken' in resp:
            try:
                resp = self.ec2_client.describe_iam_instance_profile_associations(NextToken=resp['NextToken'])
                self.ec2_iam_profiles_associations.extend(resp['IamInstanceProfileAssociations'])
            except self.ec2_client.exceptions.ClientError as e:
                logger.error(e)
                logger.error(f'Service not available in the region {self.region}')
                return
        logger.info(
            f'Done with fetching association details for {self.region}. {len(self.ec2_iam_profiles_associations)}'
            f'instance profile associations found.')

    def create_iam_map(self):
        for association in self.ec2_iam_profiles_associations:
            if association['State'] in ['associating', 'associated']:
                instance_profile_name = association['IamInstanceProfile']['Arn'].split('/')[-1]
                logger.info(f'Instance profile {instance_profile_name} is attached to '
                            f'{association["InstanceId"]} with state {association["State"]}')
                self.iam_profiles_map[association['InstanceId']] = self.iam_resource.InstanceProfile(
                    instance_profile_name)

    def update_existing_roles(self):
        for inst_id, profile in self.iam_profiles_map.items():
            logger.info(f'Checking ec2 instance {inst_id} ...')
            profile_name = profile.name
            try:
                profile.reload()
                role = profile.roles[0]
                logger.info(f'Role found {role.name}')
                if not self.contains_ssm_policies(role):
                    logger.info(f'Role {role.name} does not have all required policies.')
                    self.attach_polices(role)
                    logger.info(f'Done attaching the required policies to role {role.name}')
                logger.info(f'Done with instance {inst_id}')
                self.ec2s_with_valid_profile.add(inst_id)
            except self.iam_resource.meta.client.exceptions.NoSuchEntityException:
                self.ec2s_with_invalid_profile.add(inst_id)
                logger.error(f'Invalid instance profile {profile_name} for instance {inst_id}')
            except Exception as e:
                logger.error(f'Failed for instance {inst_id}')
                logger.error(e)
                self.unsuccessful_instances[inst_id] = e

    def get_instances_without_role(self):
        logger.info('Fetching all ec2 metadata.')
        resp = self.ec2_client.describe_instances()
        all_ec2_insts = resp['Reservations']

        while 'NextToken' in resp:
            resp = self.ec2_client.describe_instances(NextToken=resp['NextToken'])
            all_ec2_insts.extend(resp['Reservations'])
        logger.info('Done with fetching ec2 metadata.')

        for group in all_ec2_insts:
            for inst_md in group['Instances']:
                if (inst_md['State']['Name'] != 'terminated' and
                        not inst_md['InstanceId'] in self.ec2s_with_valid_profile):
                    self.ec2s_without_profile.add(inst_md['InstanceId'])

    def detach_invalid_profiles(self):

        detachment_flag = False
        # Attach lk_profile to remaining ec2
        for inst_id in list(self.ec2s_without_profile):
            if inst_id in self.ec2s_with_invalid_profile:
                detachment_flag = True
                try:
                    self.detach_profile(inst_id)
                    logger.info(f'Detaching invalid instance profile from instance {inst_id}')
                except Exception as e:
                    logger.error(f'Failed to detach instance profile for {inst_id}')
                    logger.error(e)

        if detachment_flag:
            # Give some time to do all the disassociation
            logger.info('Waiting for a minute for all the detachments to finish.')
            sleep(120)

    def attach_lk_profile_to_ec2(self, first_attempt=True):
        if first_attempt:
            remaining_ec2s = self.ec2s_without_profile
        else:
            remaining_ec2s = set(self.unsuccessful_instances)
        # Associate the lk_profile
        while remaining_ec2s:

            inst_id = remaining_ec2s.pop()
            resp = self.ec2_client.describe_iam_instance_profile_associations(
                Filters=[
                    {
                        'Name': 'instance-id',
                        'Values': [inst_id]
                    },
                ]
            )
            try:
                state = resp['IamInstanceProfileAssociations'][0]['State']
                if state in ['associating', 'associated']:
                    continue
                if state == 'disassociating':
                    sleep(20)
            except (KeyError, IndexError):
                pass
            logger.info(f'Attaching profile self.{self.lk_profile.name} with instance {inst_id}')
            try:
                self.ec2_client.associate_iam_instance_profile(
                    IamInstanceProfile={
                        'Arn': self.lk_profile.arn,
                        'Name': self.lk_profile.name
                    },
                    InstanceId=inst_id
                )
                self.unsuccessful_instances.pop(inst_id, None)
                logger.info(f'Done with instance {inst_id}')
            except botocore.exceptions.ClientError as e:
                logger.info(e)
                self.lk_profile = self.create_profile()
                self.unsuccessful_instances[inst_id] = e
        if first_attempt:
            self.attach_lk_profile_to_ec2(False)

    def log_failed_instances(self):
        if self.unsuccessful_instances:
            logger.error(self.unsuccessful_instances)
        return self.unsuccessful_instances

    def main(self):
        try:
            self.validate_region_availability()
        except RegionUnavailableError:
            return {}
        try:
            self.fetch_instance_profile_association()
            self.create_iam_map()
            self.update_existing_roles()
        except Exception as e:
            logger.error(e)
        self.get_instances_without_role()
        self.detach_invalid_profiles()
        self.attach_lk_profile_to_ec2()
        return self.log_failed_instances()
