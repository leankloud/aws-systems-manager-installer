SSM_POLICIES = [
    'arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy',
    'arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore'
]

# A role must assume a role to get temporary credentials from STS Security Token Service
# https://docs.aws.amazon.com/systems-manager/latest/userguide/automation-permissions.html#automation-role
SSM_ASSUME_ROLE_POLICY = {
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Principal": {
            "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
    }]
}

# Name of the roles to be use by the below services to perform its services
# role to attach to instance profile so that installed SSM agent will work this role is
# created by Leankloud, and will create one if not exists or uses already created one
LK_SSM_ROLE_NAME = "LeanKloudRoleForInstanceSSMAutomation"

# Details for the role
LK_ROLE_PATH = '/leankloud/'
LK_SSM_ROLE_DESCRIPTION = "Role created by LeanKloud to install SSM agent in EC2 server"


class RegionUnavailableError(Exception):
    def __init__(self, message):
        super().__init__(message)
