from agent import EC2CWRoleAssociationAgent

if __name__ == '__main__':
    unsuccessful_instances = {}
    for region in EC2CWRoleAssociationAgent.available_ec2_regions():
        print(f'Using region {region}')
        ec2_role_agent = EC2CWRoleAssociationAgent(region)
        errors = ec2_role_agent.main()
        if errors:
            unsuccessful_instances.update(errors)
            print(errors)
        print(f"Done for the {region}")

    if unsuccessful_instances:
        print('Association is unsuccessful for these instances')
        print(unsuccessful_instances)
    print('Done....')
